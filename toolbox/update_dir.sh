#!/bin/sh

if [ $# -eq 0 ]
then
    echo "You need to provide a name for the new directory"
    exit
fi

for i in server addons sync web wm
do
    echo "Update $i"
    cd ${UNIFIELDDIR}$1/unifield-$i
    bzr pull --quiet
done

cd ${UNIFIELDDIR}$1/sync_env_scripts
bzr pull --quiet

