#!/bin/sh

sudo mount /dev/sda1 -o remount,nobarrier

if [ $# -eq 0 ]
then
    echo "You need to provide a name for the new directory"
    exit
fi

if [ ! ${UNIFIELDDIR} ]
then
    echo "You should define a value for UNIFIELDDIR variable (it's the path where the files will be pushed( !";
    exit;
fi

rm_dir=0
if [ -d ${UNIFIELDDIR}$1 ]
then
    echo "The directory alreay exist !"
    echo "Would you remove this directory and the associated databases ? (y/n)";
    while true; do
        read test_rm
        case $test_rm in
            [Yy]* ) rm_dir=1; break;;
            [Nn]* ) rm_dir=0; break;;
            * ) echo "Only 'y' and 'n' are allowed";;
        esac
    done
    if [ $rm_dir -eq 1 ]
    then
        rm -r ${UNIFIELDDIR}$1;
        for i in HQ1 HQ1C1 HQ1C1P1 SYNC_SERVER;
        do
            dropdb $1_$i 2> /dev/null;
        done
    fi
    exit;
fi

createdb=0
echo "Would you create databases for this issue ? (y/n)";
while true; do
    read nodb
    case $nodb in
        [Yy]* ) createdb=1; break;;
        [Nn]* ) createdb=0; break;;
        * ) echo "Only 'y' and 'n' are allowed";;
    esac
done

update_trunk=0
echo "Would you update trunk directories ? (y/n)";
while true; do
    read trunk_up
    case $trunk_up in
        [Yy]* ) update_trunk=1; break;;
        [Nn]* ) update_trunk=0; break;;
        * ) echo "Only 'y' and 'n' are allowed";;
    esac
done

echo "WM branch [lp:unifield-wm] ?"
read unifield_wm
if [ -z "$unifield_wm" ]
then
    unifield_wm='lp:unifield-wm';
fi

echo "Addons branch [lp:unifield-addons] ?"
read unifield_addons
if [ -z "$unifield_addons" ]
then
    unifield_addons='lp:unifield-addons';
fi

echo "Server branch [lp:unifield-server] ?"
read unifield_server
if [ -z "$unifield_server" ]
then
    unifield_server='lp:unifield-server';
fi

echo "Web branch [lp:unifield-web] ?"
read unifield_web
if [ -z "$unifield_web" ]
then
    unifield_web='lp:unifield-web';
fi

echo "Sync. branch [lp:unifield-wm/sync] ?"
read unifield_sync
if [ -z "$unifield_sync" ]
then
    unifield_sync='lp:unifield-wm/sync';
fi

echo "Sync. Env. branch [lp:unifield-team/unifield-wm/sync-env] ?"
read sync_env
if [ -z "$sync_env" ]
then
    sync_env='lp:~unifield-team/unifield-wm/sync-env';
fi

if [ $update_trunk -eq 0 ]
then
    echo " 1/17 :: No update trunk"
    echo "Go to directory creation"
else
    echo " 1/17 :: Update trunk server branch"
    cd ${UNIFIELDDIR}/trunk/unifield-server
    bzr pull -q
    echo " 2/17 :: Update trunk web branch"
    cd ${UNIFIELDDIR}/trunk/unifield-web
    bzr pull -q
    echo " 3/17 :: Update trunk addons branch"
    cd ${UNIFIELDDIR}/trunk/unifield-addons
    bzr pull -q
    echo " 4/17 :: Update trunk WM branch"
    cd ${UNIFIELDDIR}/trunk/unifield-wm
    bzr pull -q
    echo " 5/17 :: Update trunk sync. branch"
    cd ${UNIFIELDDIR}/trunk/unifield-sync
    bzr pull -q
    echo " 6/17 :: Update trunk sync. script branch"
    cd ${UNIFIELDDIR}/trunk/sync_env_scripts
fi

echo " 7/17 :: Creation of the directory"
mkdir ${UNIFIELDDIR}$1

echo " 8/17 :: Enter to the directory"
cd ${UNIFIELDDIR}$1
mkdir dumps
last_exp=`ssh root@uf0003.unifield.org "ls -t1 /home/se/exports" | head -n1`


echo " 9/17 :: Download unifield-wm branch [$unifield_wm]"
bzr branch -q $unifield_wm ${UNIFIELDDIR}$1/unifield-wm

echo "10/17 :: Download unifield-addons branch [$unifield_addons]"
bzr branch -q $unifield_addons ${UNIFIELDDIR}$1/unifield-addons

echo "11/17 :: Download unifield-server branch [$unifield_server]"
bzr branch -q $unifield_server ${UNIFIELDDIR}$1/unifield-server

echo "12/17 :: Download unifield-web branch [$unifield_web]"
bzr branch -q $unifield_web ${UNIFIELDDIR}$1/unifield-web

echo "13/17 :: Download sync engine branch [$unifield_sync]"
bzr branch -q $unifield_sync ${UNIFIELDDIR}$1/unifield-sync

echo "14/17 :: Download sync scripts branch [$sync_env]"
bzr branch -q $sync_env ${UNIFIELDDIR}$1/sync_env_scripts

echo "15/17 :: Create launchers"
cp /home/openerp/.openerp_serverrc ${UNIFIELDDIR}$1/server.conf
sed -i 's/trunk/'"$1"'/g' ${UNIFIELDDIR}$1/server.conf
cp /home/openerp/.openerp-web.cfg ${UNIFIELDDIR}$1/web.conf
cp /home/openerp/unifield.config ${UNIFIELDDIR}$1/unifield-wm/unifield_tests/unifield.config
sed -i 's/couscous/'"$1"'/g' ${UNIFIELDDIR}$1/unifield-wm/unifield_tests/unifield.config
echo "
#!/bin/bash

cd ${UNIFIELDDIR}$1/unifield-server/bin;
python openerp-server.py -c ${UNIFIELDDIR}$1/server.conf \$@
" > ${UNIFIELDDIR}$1/run_server.sh
echo "
#!/bin/bash

cd ${UNIFIELDDIR}$1/unifield-web;
python openerp-web.py -c ${UNIFIELDDIR}$1/web.conf \$@
" > ${UNIFIELDDIR}$1/run_web.sh

if [ $createdb -eq 0 ]
then
    echo "17/17 :: No DB creation"
else
    echo "17/17 :: Create DB - Can take a long time"
    j=0
    for i in HQ1 HQ1C1 HQ1C1P1 SYNC_SERVER
    do
        j=$((j+1));
        echo "    $j/4 :: Get se_$i database from uf3 and restore it locally";
        ssh root@uf0003.unifield.org "su se -c 'pg_dump -Fc se_$i'" > ${UNIFIELDDIR}$1/dumps/base_$i.dump;

        createdb $1_$i;
        pg_restore -d $1_$i < ${UNIFIELDDIR}$1/dumps/base_$i.dump 2> /dev/null;
        psql $1_$i -c "UPDATE sync_client_sync_server_connection SET database = '$1_SYNC_SERVER', host = 'localhost', port=8070" 2> /dev/null;
        #sh ${UNIFIELDDIR}$1/run_server.sh -d $1_$i -u base --stop-after-init;
    done

    #psql $1_SYNC_SERVER -c "UPDATE sync_server_entity SET hardware_id = 'da646368c37b72b9f0005b3ce7419733'";
    #psql $1_SYNC_SERVER -c "UPDATE sync_server_entity SET hardware_id = '56cfb948109e6cb3722238b0e40af968'";
    #psql $1_SYNC_SERVER -c "UPDATE sync_server_entity SET hardware_id = '7acf1f112779fd5662a9c440f2d100e3'" 2> /dev/null;
    #psql $1_SYNC_SERVER -c "UPDATE sync_server_entity SET hardware_id = '7c42a0568c26b9a951b9ff121ce7aad6'" 2> /dev/null;
    #psql $1_SYNC_SERVER -c "UPDATE sync_server_entity SET hardware_id = '86886b9421df2d690f9a12df63240e18'" 2> /dev/null;
    psql $1_SYNC_SERVER -c "UPDATE sync_server_entity SET hardware_id = '32a4e9f2485c602e6bdb1e7bf6ba7d78'" 2> /dev/null;
    #psql $1_SYNC_SERVER -c "UPDATE sync_server_entity SET hardware_id = 'efc21c04a18e1ceab395de571c8296e9'" 2> /dev/null;
    pg_dump -Fc $1_SYNC_SERVER > ${UNIFIELDDIR}$1/dumps/base_SYNC_SERVER.dump;
    #sudo mount /dev/sda6 -o remount,barrier
    for i in HQ1 HQ1C1 HQ1C1P1 SYNC_SERVER
    do
        pg_dump -Fc $1_$i > ${UNIFIELDDIR}$1/dumps/base_$i.dump;
    done
fi
