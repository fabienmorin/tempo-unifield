tempo-unifield
--------------

This repo will contain all custom files, like personal scripts, file
configuration, ... This is made to have an history of the changes and to get 
a backup


modify the hardware-id
----------------------
Hardware ID is unique per machine and should be set in the instance. The first
time you run the openerp server, if the ID set in the instance does not match
with your ID, a warning message is display, giving you the right ID
corresponding to you machine. You can then change the ID in the database doing:

```
openerp@ubuntu-unifield:~$ psql test_avec_base_SYNC_SERVER
test_avec_base_SYNC_SERVER=# update sync_server_entity set hardware_id = '86886b9421df2d690f9a12df63240e18';
UPDATE 4
test_avec_base_SYNC_SERVER=# \q
```